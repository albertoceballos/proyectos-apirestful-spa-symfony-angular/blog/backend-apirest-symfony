<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Firebase\JWT\JWT;
//modelo
Use App\Entity\User;

class JwtAuth extends AbstractController {

    public $key;

    public function __construct() {
        $this->key = "mi_clave_secretísima1000$##";
    }

 
    public function signUp($email, $password, $getToken = false) {

        //comprobar el email y el password proporcionados

        $em = $this->getDoctrine()->getManager();
        $user_repo = $em->getRepository(User::class);
        $user = $user_repo->findOneBy(['email' => $email, 'password' => $password]);
        
      

        //si el usuario es correcto
        if (!empty($user) && is_object($user)) {

            //preparar token
            $token = [
                'sub' => $user->getId(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'email' => $user->getEmail(),
                //tiempo unix de creación del token  
                'iat' => time(),
                //Caducidad del token en una semana   
                'exp' => time() + (60 * 60 * 24 * 7),
                'description'=>$user->getDescription(),
                'image' => $user->getImage(),
                'createdAt'=>$user->getCreatedAt(),
                'updatedAt'=>$user->getUpdatedAt(),
            ];
            //codifico el token
            $jwt = JWT::encode($token, $this->key, 'HS256');

            //si getToken no viene, me devuelve los datos decodificados
            if (empty($getToken)) {
                $decoded = JWT::decode($jwt, $this->key,['HS256']);
                $dataLogin = $decoded;
            } else {
                //si getToken viene a true, me devuelve el token   
                $dataLogin = $jwt;
            }
            
            $data=[
               'code' => 200,
                'status' => 'success',
                'message' => 'Login correcto',
                'data'=>$dataLogin,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Login incorrecto',
            ];
        }


        return $data;
    }
    
    //comprobar token
    public function checkToken($jwt,$identity=false){
        $auth=false;
        
        try {
            $decoded= JWT::decode($jwt, $this->key,['HS256']);
        } catch (\UnexpectedValueException $exc) {
            $auth=false;
        } catch (\DomainException $exc){
            $auth=false;
        }
        
        //si el token es correcto mando $auth a true
        if(!empty($decoded)){
            $auth=true;
        }
        
        //sie identity viene a  true y el token es correcto devuelvo la identidad
        if($auth==true && $identity!=false){
             $auth=$decoded;
        }
        
        return $auth;
        
    }

}
