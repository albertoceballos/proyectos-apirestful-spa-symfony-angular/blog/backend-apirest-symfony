<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//modelo
use App\Entity\Category;
//servicio para autenticación
use App\Service\JwtAuth;

class CategoryController extends AbstractController {

    public function resJson($data) {
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    //sacar categorías
    public function index() {
        $category_repo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $category_repo->findAll();

        if (!empty($categories)) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'categories' => $categories,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No hay categorías',
            ];
        }

        return $this->resJson($data);
    }

    //Guardar nueva categoría
    public function add(Request $request, JwtAuth $jwtAuthService) {


        //conseguir token de la cabecera
        $token = $request->headers->get('Authorization');
        //comprobar token
        $auth = $jwtAuthService->checkToken($token);

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se pudo crear la nueva categoría',
        ];

        //si la autenticación es válida,permitir añadir nueva categoría
        if ($auth) {
            //conseguir datos de POST de la nueva categoría a crear
            $json = $request->get('json');
            $params = json_decode($json);
            if (!empty($json)) {
                //validar datos
                $name = !empty($params->name) ? $params->name : null;
                if (!empty($name)) {
                    //crear nuevo objeto de Categoría
                    $category = new Category();
                    $category->setName($name);
                    $category->setCreatedAt(new \DateTime('now'));
                    //persistir y guardar
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($category);
                    $em->flush();

                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Categoría creada con éxito',
                        'category' => $category,
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }

        return $this->resJson($data);
    }

    //actualizar categoría
    public function update($id, Request $request, JwtAuth $jwtAuthService) {

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se pudo actualizar la categoría',
        ];

        //conseguir token de la cabecera
        $token = $request->headers->get('Authorization');
        //comprobar token
        $auth = $jwtAuthService->checkToken($token);

        //si la autenticación es buena     
        if ($auth) {
            //buscar la categoría por el id que llega por parámetro
            $category_repo = $this->getDoctrine()->getRepository(Category::class);
            $category = $category_repo->find($id);

            //si existe la categoría
            if (!empty($category) && is_object($category)) {

                //recoger datos de POST
                $json = $request->get('json');
                $params = json_decode($json);

                if (!empty($json)) {
                    //validar datos
                    $name = !empty($params->name) ? $params->name : null;
                    if (!empty($name)) {
                        //setear los datos nuevos
                        $category->setName($name);
                        $category->setUpdatedAt(new \DateTime('now'));

                        //persistir y guardar
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($category);
                        $em->flush();

                        $data = [
                            'code' => 200,
                            'status' => 'success',
                            'message' => 'Categoría actualizada con éxito',
                            'category' => $category,
                        ];
                    }
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }

        return $this->resJson($data);
    }
    
    public function detail($id){
        
        $data=[
          'code'=>400,
          'status'=>'error',
          'message'=>'No se encontró la categoría',
            
        ];
        $cat_repo= $this->getDoctrine()->getRepository(Category::class);
        $cat=$cat_repo->find($id);
        
        if(!empty($cat)){
            $data=[
              'code'=>200,
              'status'=>'success',
              'category'=>$cat,  
            ];
        }
        
        return $this->resJson($data);
    }

}
