<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//para devolver una imagen
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
//modelo
use App\Entity\Post;
use App\Entity\Category;
use App\Entity\User;
//servicio
use App\Service\JwtAuth;

class PostController extends AbstractController {

    //función para serializar todos los datos a JSON;
    public function resJson($data) {

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    //todos los posts
    public function index() {
        $posts_repo = $this->getDoctrine()->getRepository(Post::class);
        $posts = $posts_repo->findAll();

        $data = [
            'status' => 'success',
            'code' => 200,
            'posts' => $posts,
        ];

        return $this->resJson($data);
    }

    //Guardar un nuevo post
    public function add(Request $request, JwtAuth $jwtAuthService) {

        //sacar token y comrpobar
        $token = $request->headers->get('Authorization');
        $auth = $jwtAuthService->checkToken($token);

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se pudo crear el nuevo post',
        ];

        //si es válido sacar la identidad
        if ($auth) {
            $identity = $jwtAuthService->checkToken($token, true);

            $users_repo = $this->getDoctrine()->getRepository(User::class);
            $user = $users_repo->find($identity->sub);

            //recoger datos de la publicación a crear desde POST
            $json = $request->get('json');
            $params = json_decode($json);
            //validar datos
            if (!empty($json)) {

                $title = !empty($params->title) ? $params->title : null;
                $content = !empty($params->content) ? $params->content : null;
                $category = !empty($params->category) ? $params->category : null;
                $image = !empty($params->image) ? $params->image : null;

                //comprobar que la categoría existe
                $cat_repo = $this->getDoctrine()->getRepository(Category::class);
                $cat = $cat_repo->find($category);

                if (empty($cat)) {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'La categoría no existe',
                    ];
                }

                //si todo es correcto crear el nuevo objeto Post
                if (!empty($title) && !empty($content) && !empty($cat)) {
                    $post = new Post();
                    //setear los valores
                    $post->setUser($user);
                    $post->setCategory($cat);
                    $post->setTitle($title);
                    $post->setContent($content);
                    $post->setImage($image);
                    $post->setCreatedAt(new \DateTime('now'));

                    //persistir y guardar en la BB.DD
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($post);
                    $em->flush();

                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Nuevo post crado con éxito',
                        'post' => $post,
                    ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Faltan datos',
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }

        return $this->resJson($data);
    }

    //actualizar un post
    public function update(Request $request, JwtAuth $jwtAuthService, $id) {

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se pudo actualizar el post',
        ];
        //conseguir token
        $token = $request->headers->get('Authorization');
        //comprobar validez del token
        $auth = $jwtAuthService->checkToken($token);
        //si es válido el token sacar identidad de usuario 
        if ($auth) {
            $identity = $jwtAuthService->checkToken($token, true);
            $userId = $identity->sub;
            //sacar publicacion por id comprobando que la publicacion pertenece al usuario
            $post_repo = $this->getDoctrine()->getRepository(Post::class);
            $post = $post_repo->findOneBy(['id' => $id, 'user' => $userId]);

            if (!empty($post)) {
                //si es del usuario, conseguir datos a actualizar
                $json = $request->get('json');
                $params = json_decode($json);

                if (!empty($json)) {
                    //validar datos a actualizar   
                    $title = !empty($params->title) ? $params->title : null;
                    $content = !empty($params->content) ? $params->content : null;
                    $category = !empty($params->category) ? $params->category : null;

                    //compruebo que la categoría existe
                    $category_repo = $this->getDoctrine()->getRepository(Category::class);
                    $cat = $category_repo->find($category);
                    if (empty($cat)) {
                        $data = [
                            'code' => 200,
                            'status' => 'error',
                            'message' => 'La categoría no existe',
                        ];
                    }
                    if (!empty($title) && !empty($content) && !empty($cat)) {
                        //settear nuevos datos al objeto de la publicación
                        $post->setTitle($title);
                        $post->setContent($content);
                        $post->setCategory($cat);
                        $post->setUpdatedAt(new \DateTime('now'));

                        //persistir y guardar en la BB.DD
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($post);
                        $em->flush();

                        //devolver datos
                        $data = [
                            'code' => 200,
                            'status' => 'success',
                            'message' => 'Post actualizado con éxito',
                            'post' => $post,
                        ];
                    } else {
                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'message' => 'Faltan datos',
                        ];
                    }
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación no válida',
            ];
        }

        return $this->resJson($data);
    }

    //borrar un post
    public function delete(Request $request, JwtAuth $jwtAuthService, $id) {
        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se pudo borrar el post',
        ];
        //conseguir token
        $token = $request->headers->get('Authorization');
        //comprobar validez del token
        $auth = $jwtAuthService->checkToken($token);
        //si es válido el token sacar identidad de usuario 
        if ($auth) {
            $identity = $jwtAuthService->checkToken($token, true);
            $userId = $identity->sub;
            //sacar publicacion por id comprobando que la publicacion pertenece al usuario
            $post_repo = $this->getDoctrine()->getRepository(Post::class);
            $post = $post_repo->findOneBy(['id' => $id, 'user' => $userId]);

            //si existe la publicación y es del usuario
            if (!empty($post)) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($post);
                $em->flush();
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'publicación borrada con éxito',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación no válida',
            ];
        }
        return $this->json($data);
    }

    //subir imagen de publicación
    public function uploadImage(Request $request) {
        $file = $request->files->get('file0');


        if (!empty($file)) {
            $extension = $file->guessExtension();
            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png') {

                $file_name = md5(uniqid()) . "." . $file->guessExtension();
                $file->move('uploads/posts', $file_name);

                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'file' => $file_name,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Extensión no válida',
                ];
            }
        }
        return $this->resJson($data);
    }

    //devolver imagen
    public function getImage(Filesystem $filesystem, $filename) {

        $exist_file = $filesystem->exists('uploads/posts/' . $filename);

        if ($exist_file) {

            $file = new File('uploads/posts/' . $filename);
            return $this->file($file);
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No existe el archivo',
            ];
        }
        return $this->json($data);
    }

    //Detalle de un post
    public function detail($id) {

        $posts_repo = $this->getDoctrine()->getRepository(Post::class);
        $post = $posts_repo->find($id);

        if (!empty($post)) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'post' => $post,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No existe el post',
            ];
        }

        return $this->resJson($data);
    }

    //Listar posts de un usuario
    public function postsByUser($userId) {

        $posts_repo = $this->getDoctrine()->getRepository(Post::class);
        $posts = $posts_repo->findBy(['user' => $userId]);

        if (!empty($posts)) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'posts' => $posts,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No se encontraron publicaciones del usuario',
            ];
        }

        return $this->resJson($data);
    }

    //Listar posts de una categoría
    public function postsByCategory($catId) {
        $posts_repo = $this->getDoctrine()->getRepository(Post::class);
        $posts = $posts_repo->findBy(['category' => $catId]);

        if (!empty($posts)) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'posts' => $posts,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No se encontraron publicaciones de la categoría',
            ];
        }
        return $this->resJson($data);
    }

    //Buscar posts por título o contenido
    public function searchPosts($string) {

        //datos por defecto
        $data = [
            'status' => 'error',
            'code' => 400,
            'message' => 'No se encontró ningún post con esos parámetros',
        ];

        $dql = "SELECT p FROM App\Entity\Post p WHERE p.title LIKE :string OR p.content LIKE :string";
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql)->setParameter('string', '%'.$string.'%');

        $result = $query->execute();

        if (!empty($result)) {
            $data = [
                'status' => 'success',
                'code' => 200,
                'posts' => $result,
            ];
        }

        return $this->resJson($data);
    }

}
