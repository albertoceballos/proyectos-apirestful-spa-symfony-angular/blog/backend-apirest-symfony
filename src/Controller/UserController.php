<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
Use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//validacion de campos
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
//Modelo
use App\Entity\User;
//servicios
use App\Service\JwtAuth;

class UserController extends AbstractController {

    //función para serializar todos los datos a JSON;
    public function resJson($data) {

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function index() {

        $users_repo = $this->getDoctrine()->getRepository(User::class);
        $users = $users_repo->findAll();


        $data = [
            'status' => 'success',
            'code' => 200,
            'users' => $users,
        ];

        return $this->resJson($data);
    }

    public function register(Request $request) {
        //sacar datos de post y decodificar desde json
        $json = $request->get('json');
        $params = json_decode($json);

        //datos por defecto
        $data = [
            'status' => 'error',
            'code' => 200,
            'message' => 'Usuario no registrado',
        ];
        //validar datos
        $name = (!empty($params->name)) ? $params->name : null;
        $surname = (!empty($params->surname)) ? $params->surname : null;
        $email = (!empty($params->email)) ? $params->email : null;
        $password = (!empty($params->password)) ? $params->password : null;
        $description = (!empty($params->description)) ? $params->description : null;

        //Comprobar Email válido
        $validator = Validation::createValidator();
        $email_validation = $validator->validate($email, [
            new Email(),
        ]);
        //comprobar tamaño mínimo de password
        $password_validation = $validator->validate($password, [
            new Length(['min' => 5]),
        ]);

        if ($name != null && $email != null && count($email_validation) == 0 && $password != null && count($password_validation) == 0) {
            //creo nuevo objeto de usuario
            $user = new User();
            //asigno valores al objeto
            $user->setName($name);
            $user->setSurname($surname);
            $user->setEmail($email);
            $user->setRole('ROLE_USER');
            $user->setDescription($description);
            $user->setCreatedAt(new \DateTime('now'));

            //encriptar password
            $pwd = hash("sha256", $password);
            $user->setPassword($pwd);
            //comprobar que no hay E-mail duplicados en la BBDD
            $user_repo = $this->getDoctrine()->getRepository(User::class);
            $isset_email = $user_repo->findBy(['email' => $email]);
            if (empty($isset_email)) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario registrado con éxito',
                    'user' => $user,
                ];
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Error, el e-mail de usuario ya está registrado',
                ];
            }
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'Validación de datos incorrecta',
            ];
        }

        return $this->resJson($data);
    }

    public function login(JwtAuth $jwtAuth, Request $request) {

        //recoger datos de POST
        $json = $request->get('json');
        $params = json_decode($json);

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se ha podido identificar al usuario',
        ];


        if (!empty($json)) {
            //validar datos
            $email = !empty($params->email) ? $params->email : null;
            $password = !empty($params->password) ? $params->password : null;
            $getToken = !empty($params->getToken) ? $params->getToken : null;

            //validación del email
            $validation = Validation::createValidator();
            $validation_email = $validation->validate($email, [
                new Email(),
            ]);
            //validación de la contraseña
            $validation_password = $validation->validate($password, [
                new Length(['min' => 5]),
            ]);
            //si los datos vienen bien
            if (!empty($email) && count($validation_email) == 0 && !empty($password) && count($validation_password) == 0) {

                //codificar contraseña
                $pwd = hash('sha256', $password);

                //llamar al servicio que me comprueba el login y me devuelve el token o los datos según le mandemos o no getToken
                if (empty($getToken)) {
                    $login = $jwtAuth->signUp($email, $pwd);
                } else {
                    $login = $jwtAuth->signUp($email, $pwd, $getToken);
                }

                return new JsonResponse($login);
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'La validación de los datos no es correcta',
                ];
            }
        }

        return new JsonResponse($data);
    }

    //actualizar datos de usuario

    public function update(Request $request, JwtAuth $jwtAuthService) {

        //recoger token de la cabcera
        $token = $request->headers->get('Authorization');

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se ha podido actualizar el usuario',
        ];

        //comprobar token
        $auth = $jwtAuthService->checkToken($token);
        //si el servicio devuelve true, es correcto el token
        if ($auth) {
            //vuelvo a llamar al servicio mandandole identity con true, para que me devuelva la identidad
            $identity = $jwtAuthService->checkToken($token, true);

            //buscar el usuario en la BBDD
            $em = $this->getDoctrine()->getManager();
            $user_repo = $em->getRepository(User::class);
            $user = $user_repo->find($identity->sub);

            //recoger parámetros para actualizar
            $json = $request->get('json');
            $params = json_decode($json);

            if (!empty($json)) {
                //validar datos
                $name = (!empty($params->name)) ? $params->name : null;
                $surname = (!empty($params->surname)) ? $params->surname : null;
                $email = (!empty($params->email)) ? $params->email : null;
                $description = (!empty($params->description)) ? $params->description : null;

                //Comprobar Email válido
                $validator = Validation::createValidator();
                $email_validation = $validator->validate($email, [
                    new Email(),
                ]);

                if (!empty($name) && !empty($email) && count($email_validation) == 0) {

                    ///setear los nuevos parámetros al objeto user
                    $user->setName($name);
                    $user->setSurname($surname);
                    $user->setEmail($email);
                    $user->setDescription($description);
                    $user->setUpdatedAt(new \DateTime('now'));

                    //comprobar que el email no esté duplicado
                    $isset_email = $user_repo->findBy(['email' => $email]);
                    if (empty($isset_email) || $email == $identity->email) {
                        //persistir el objeto y guardar en la BBDD
                        $em->persist($user);
                        $em->flush();

                        $data = [
                            'code' => 200,
                            'status' => 'success',
                            'message' => 'Usuario actualizado con éxito',
                            'user' => $user,
                        ];
                    } else {
                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'message' => "El email $email ya está en uso, usa otro por favor",
                        ];
                    }
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Validación de datos incorrecta',
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }

        return new JsonResponse($data);
    }

    //subir imagen de usuario
    public function uploadAvatar(Request $request, JwtAuth $jwtAuthService) {

        //datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'No se ha podido subir la imagen de usuario',
        ];
        //recoger token de la cabecera
        $token = $request->headers->get('Authorization');
        //comprobar token
        $auth = $jwtAuthService->checkToken($token);
        //si es correcto
        if ($auth) {
            //sacar identidad del usuario con el token
            $identity = $jwtAuthService->checkToken($token, true);

            //buscar el usuario
            $em = $this->getDoctrine()->getManager();
            $user_repo = $em->getRepository(User::class);
            $user = $user_repo->find($identity->sub);

            //conseguir datos de imagen a subir
            $file = $request->files->get('file0');

            if (!empty($file)) {
                //comprobar la extensión del archivo
                $extension = $file->guessExtension();
                if ($extension == "jpg" || $extension == "jpeg" || $extension == "gif" || $extension == "png") {
                    //si todo es corecto, renombro el archivo
                    $file_name = $user->getId() . time() . "." . $extension;
                    //subida del archivo
                    $file->move('uploads/users', $file_name);
                    //setear el nombre de archivo de la imagen al obejto usuario
                    $user->setImage($file_name);
                    //persistir y guardar
                    $em->persist($user);
                    $em->flush();
                    $data=[
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Avatar de usuario subido con éxito',
                        'user'=>$user,
                    ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'El formato de imagen no es válido',
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }
        return new JsonResponse($data);
        
    }

    //detalles de un usuario por el id
    public function detail($id){
        
        $user_repo= $this->getDoctrine()->getManager()->getRepository(User::class);
        $user=$user_repo->find($id);
        if(!empty($user)){
            $data=[
              'code'=>200,
              'status'=>'success',
              'user'=>$user,
            ];
        }else{
            $data=[
              'code'=>400,
              'status'=>'error',
              'message'=>'No se ha podido encontrar el usuario',
            ];
        }
        
        return $this->resJson($data);
    }
    
}
